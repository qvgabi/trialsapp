from sqlalchemy.orm import Session
# from sqlalchemy.sql.sqltypes import Date
from datetime import date, time
import models
import schemas
from jose import JWTError, jwt
from passlib.context import CryptContext
from datetime import datetime, timedelta
from typing import Optional
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from fastapi import FastAPI, Form, Depends, UploadFile, File, HTTPException, status

SECRET_KEY = "dc571de754fd34afd3a51de95ac42198f3bd96da1c3526cd0281840ac917923f"
ALGORITHM = "HS256"

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")

def verify_password(plain_password, hashed_password):
    return pwd_context.verify(plain_password, hashed_password)

def get_password_hash(password):
    return pwd_context.hash(password)

def authenticate_user(db, username: str, password: str):
    user = get_user(db, username)
    if not user:
        return False
    if not verify_password(password, user.hashed_password):
        return False
    return user

def create_access_token(data: dict, expires_delta: Optional[timedelta] = None):
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=15)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
    return encoded_jwt



def get_user(db: Session, username: str):
    return db.query(models.User).filter(models.User.username == username).first()


def get_user_by_email(db: Session, email: str):
    return db.query(models.User).filter(models.User.email == email).first()


def get_users(db: Session, skip: int = 0, limit: int = 10):
    return db.query(models.User).offset(skip).limit(limit).all()


def get_bookings(user_id: int, db: Session, skip: int = 0, limit: int = 10):
    return db.query(models.Booking).filter(models.Booking.patient_id == user_id).offset(skip).limit(limit).all()

def get_trials(booking_id: int, db: Session, skip: int = 0, limit: int = 10):
    return db.query(models.Trial).filter(models.Trial.booking_id == booking_id).offset(skip).limit(limit).all()


def create_user(db: Session, user: schemas.UserCreate):
    db_user = models.User(username = user.username, email = user.email, hashed_password=get_password_hash(user.password))
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user


def create_user_booking(db: Session, Booking: schemas.BookingCreate, user_id: int):
    db_booking = models.Booking(**Booking.dict(), patient_id=user_id)
    db.add(db_booking)
    db.commit()
    db.refresh(db_booking)
    return db_booking

def update_user_booking(db: Session, booking_id: int, date: date):
    db_booking = db.query(models.Booking).filter(models.Booking.id == booking_id).first()
    db_booking.date = date
    db.commit()
    db.refresh(db_booking)
    return db_booking

def cancel_booking(db:Session, booking_id: int):
    db_booking = db.query(models.Booking).filter(models.Booking.id == booking_id).first()

    if db_booking is None: 
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Resource Not Found")


    db.delete(db_booking)
    db.commit()
    
    return db_booking
    
def register_trial(db:Session, Trial: schemas.TrialCreate, booking_id: int):
    db_trial = models.Trial(**Trial.dict(), booking_id=booking_id)
    db.add(db_trial)
    db.commit()
    db.refresh(db_trial)
    return db_trial

def send_images(db:Session, trial_id: int, url: str):
    db_trial = db.query(models.Trial).filter(models.Trial.id == trial_id).first()
    db_trial.url = url
    db.commit()
    db.refresh(db_trial)
    return db_trial
# def create_reservation(db: Session, )
