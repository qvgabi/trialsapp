# from sqlalchemy.sql.sqltypes import Date, Time
from pydantic import BaseModel
from typing import List, Optional
from datetime import date, time

class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    username: Optional[str] = None


class UserBase(BaseModel):
    username: str
    


class BookingBase(BaseModel):
    title: str
    date: date
    start: time
    end: time
    cycle_day: int
    description: str

    class Config:
        arbitrary_types_allowed = True

class TrialBase(BaseModel):
    date: date
    description: str
    cycle_day: int
    # url: str



class UserCreate(UserBase):
    password: str
    email: Optional[str] = None


class BookingCreate(BookingBase):
    pass

class TrialCreate(TrialBase):
    pass
    

class Booking(BookingBase):
    id: int
    patient_id: int
    # url: int

    class Config:
        orm_mode = True


class User(UserBase):
    id: int
    is_active: Optional[bool] = None
    hashed_password: str
    Bookings: List[Booking] = []

    class Config:
        orm_mode = True

class Trail(TrialBase):
    id: int
    booking_id: int
    url: Optional[str] = None

    class Config:
        orm_mode = True
