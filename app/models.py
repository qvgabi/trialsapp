from sqlalchemy import Boolean, Column, ForeignKey, Integer, String, Date
from sqlalchemy.orm import relationship
from sqlalchemy.sql.sqltypes import Time
from database import Base
from sqlalchemy_utils import URLType
# from sqlalchemy.ext.declarative import declarative_base


# Base = declarative_base()

class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True, index=True)
    username = Column(String)
    email = Column(String, unique=True, index=True)
    hashed_password = Column(String)
    is_active = Column(Boolean, default=True)

    bookings = relationship("Booking", back_populates="patient")


class Booking(Base):
    __tablename__ = "bookings"

    id = Column(Integer, primary_key=True, index=True)
    title = Column(String)
    date = Column(Date)
    start = Column(Time)
    end = Column(Time)
    description = Column(String, index=True)
    cycle_day = Column(Integer)
    # image
    patient_id = Column(Integer, ForeignKey("users.id"))

    patient = relationship("User", back_populates="bookings")
    trials = relationship("Trial", back_populates="bookings")


class Trial(Base):
    __tablename__ = "trials"

    id = Column(Integer, primary_key=True, index=True)
    date = Column(Date)
    description = Column(String, index=True)
    cycle_day = Column(Integer)
    url = Column(String)
    # image
    booking_id = Column(Integer, ForeignKey("bookings.id"))

    bookings = relationship("Booking", back_populates="trials")
