from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
import psycopg2

SQLALCHEMY_DATABASE_URL = "postgresql://postgres:postgres@db:5432/test_db"
# SQLALCHEMY_DATABASE_URL = "postgresql://postgres:postgres@localhost/fastapi_db"

# SQLALCHEMY_DATABASE_URL = "sqlite:///./sql_app.db"

# conn_string = "host='localhost' dbname='fastapi_db' user='postgres' password='secret'"
# conn = psycopg2.connect(conn_string)

engine = create_engine(
    SQLALCHEMY_DATABASE_URL,
)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()
