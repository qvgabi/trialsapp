from os import confstr_names
from fastapi import FastAPI, Form, Depends, UploadFile, File, HTTPException, status
import schemas, crud, models, database
from sqlalchemy.orm import Session
import uvicorn
from typing import List
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
import shutil
from jose import JWTError, jwt
from passlib.context import CryptContext
from datetime import datetime, timedelta
from datetime import date, time
import os
from minio import Minio
from minio.error import S3Error
import numpy as np
from io import BytesIO
import io
import matplotlib.pyplot as plt
from PIL import Image

import cv2

models.Base.metadata.create_all(bind=database.engine)

app = FastAPI()

ACCESS_TOKEN_EXPIRE_MINUTES = 30
pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")

def get_db():
    db = database.SessionLocal()
    try:
        yield db
    finally:
        db.close()

client = Minio(
        endpoint="minio:9000",
        access_key="minioadmingabi",
        secret_key="minioadmingabi",
        secure = False
    )



async def get_current_user(db: Session = Depends(get_db), token: str = Depends(oauth2_scheme)):
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        payload = jwt.decode(token, crud.SECRET_KEY, algorithms=[crud.ALGORITHM])
        username: str = payload.get("sub")
        if username is None:
            raise credentials_exception
        token_data = schemas.TokenData(username=username)
    except JWTError:
        raise credentials_exception
    user = crud.get_user(db, username=token_data.username)
    if user is None:
        raise credentials_exception
    return user


async def get_current_active_user(current_user: schemas.User = Depends(get_current_user)):
    if current_user.disabled:
        raise HTTPException(status_code=400, detail="Inactive user")
    return current_user


@app.post("/token", response_model=schemas.Token)
async def login_for_access_token(db: Session = Depends(get_db), form_data: OAuth2PasswordRequestForm = Depends()):
    user = crud.authenticate_user(db, form_data.username, form_data.password)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
    access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = crud.create_access_token(
        data={"sub": user.username}, expires_delta=access_token_expires
    )
    return {"access_token": access_token, "token_type": "bearer"}

# PACJENTKA FUNKCJE

@app.post("/users/")
def create_user(user: schemas.UserCreate, db: Session = Depends(get_db)):
    if crud.get_user_by_username(db, user.username):
        raise HTTPException(status_code=400, detail="Username already registered")
    if crud.get_user_by_email(db, user.email):
        raise HTTPException(status_code=400, detail="Email already registered")

    return crud.create_user(db=db, user=user)


@app.post("/{userid}/todo")
async def create_booking(booking: schemas.BookingCreate, userid: int, db: Session = Depends(get_db)):
    booking = crud.create_user_booking(db, booking, user_id=userid)
    return booking



# @app.get("/users/me/", response_model=schemas.User)
# async def read_users_me(current_user: schemas.User = Depends(get_current_active_user)):
#     return current_user

@app.get("/users/")
def get_users(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    users = crud.get_users(db, skip, limit)
    return users



@app.get("/{userid}/todo")
def get_bookings(user_id: int, skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    bookings = crud.get_bookings(user_id, db, skip, limit)
    return bookings

@app.get("/{booking_id}")
def get_trials(booking_id: int, skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    trials = crud.get_trials(booking_id, db, skip, limit)
    return trials

@app.post("/uploadfile/")
def create_upload_file(trial_id: int, image: UploadFile = File(...)):

    # file_name = os.getcwd()+"/media/"+str(trial_id)
    bucket_name = "trial" + str(trial_id)
    object_name = image.filename

    found = client.bucket_exists(bucket_name)
    if not found:
        client.make_bucket(bucket_name)
    else:
        print(f'Bucket {bucket_name} already exists')


    print(client.fput_object(bucket_name, object_name, image.file.fileno()))

    # # Get a full object
    # data = client.get_object(bucket_name, object_name)
    # with open('newfile', 'wb') as file_data:
    #     for d in data:
    #         file_data.write(d)
    # file_data.close()

    # object_data = client.get_object(bucket_name, object_name)
    # print(object_data.size)

    # Get a full object locally.
    # print(client.fget_object(bucket_name, object_name, 'newfile-f'))
    

    return {"filename": image.filename}

@app.put("/{booking_id}")
async def update_booking(booking_id: int, date: date, db: Session = Depends(get_db)):
    booking = crud.update_user_booking(db, booking_id, date)
    return booking

@app.delete("/{booking_id}")
async def cancel_booking(booking_id: int, db: Session = Depends(get_db)):
    canceled = crud.cancel_booking(db, booking_id)
    return canceled

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)

@app.post("/{trial_id}")
async def register_trial(trial: schemas.TrialCreate, booking_id: int, db: Session = Depends(get_db)):
    trial = crud.register_trial(db, trial, booking_id)
    return trial



@app.get("/image/")
async def get_trial_images(trial_id: int):
    bucket_name = "trial" + str(trial_id)
    new_bucket_name = bucket_name + "proc"


    found = client.bucket_exists(new_bucket_name)
    if not found:
        client.make_bucket(new_bucket_name)
    else:
        print(f'Bucket {new_bucket_name} already exists')

    objects = client.list_objects(bucket_name)

    for obj in objects:
        print(obj.object_name)
        object_name = obj.object_name
        
        # new_object_name = "bw"+object_name

        try:
            response = client.get_object(bucket_name, object_name)
            data = response.read()
        finally:
            response.close()
            response.release_conn()

        data_in = BytesIO(data)
        image = Image.open(data_in)
        open_cv_image = np.array(image)
        gray = cv2.cvtColor(open_cv_image, cv2.COLOR_BGR2GRAY)

        ret, buf = cv2.imencode('.jpeg', gray)

        buf = buf.tobytes()
        data = io.BytesIO(buf)
        length = data.getbuffer().nbytes
        
        client.put_object(
            bucket_name=new_bucket_name,
            object_name=object_name,
            data=data,
            length=length)


    return bucket_name

@app.get("/buckets/")
def printing_buckets():
    buckets = client.list_buckets()
    for bucket in buckets:
        if bucket.name != "app":
            objects = client.list_objects(bucket.name)
            for obj in objects:
                print(obj.bucket_name, obj.object_name)
                # client.remove_object(obj.bucket_name, obj.object_name)
            print(bucket.name)
        # client.remove_bucket(bucket.name)

