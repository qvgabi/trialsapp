FROM python:3.7

RUN apt-get update
RUN apt install -y libgl1-mesa-glx

COPY requirements.txt /app/requirements.txt

WORKDIR /app/

# set env variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN pip install -r requirements.txt

COPY app .




# EXPOSE 8000